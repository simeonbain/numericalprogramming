/***************************************************************************
 *
 *   File        : tasks.c
 *   Student Id  : 638697
 *   Name        : Simeon Bain
 *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <string.h>
#include "tasks.h"

/* Task 1 Constants */
#define MAX_VEL_DIFF_MIN_X 20.0000

/* Task 2 Constants */
#define COARSE_GRID_INIT_POINTS_IN_CELL_LENGTH 100
#define COARSE_GRID_MIN_X 10.0000
#define COARSE_GRID_MAX_X 70.0000
#define COARSE_GRID_MIN_Y -20.0000
#define COARSE_GRID_MAX_Y 20.0000

/* Task 3 Constants */
#define VEL_STAT_MIN_THRESHOLD 5
#define VEL_STAT_THRESHOLD_DIVIDER 10.0
#define VEL_STAT_MAX_THRESHOLD 11
#define FRACTION_TO_PERCENT 100.0

/* Task 4 Constants */
#define WAKE_VIS_MIN_X 10
#define WAKE_VIS_MAX_X 65
#define WAKE_VIS_X_INCREMENT 5
#define WAKE_VIS_BOUND 0.05000

void maxveldiff(point_t* points, int num_points) {   

    /* Remove points outside the desired x range */ 
    for (int i = 0; i < num_points; i++) {

        if (points[i].x < MAX_VEL_DIFF_MIN_X) {
            // swap with last element and decrement array size
            swappoints(&points[i], &points[num_points-1]);
            num_points--; 
            i--; // check the swapped element element also
        }
    }

    /* Make sure we have points in the domain */ 
    if (num_points <= 0) {
        printf("ERROR: no points in 'Max Velocity Difference' domain\n");
        return; 
    }
    
    /* Find points with max difference in u */              
    quicksortpoints(points, 0, num_points-1, &ucompare); // first sort by ascending u
    point_t min_u = maxveldiff_minfromduplicates(points, num_points, &ucompare); // grab min from start
    point_t max_u = maxveldiff_maxfromduplicates(points, num_points, &ucompare); // grab max from end

    /* Find points with max difference in v */ 
    quicksortpoints(points, 0, num_points-1, &vcompare); // first sort by ascending v
    point_t min_v = maxveldiff_minfromduplicates(points, num_points, &vcompare); // grab min from start     
    point_t max_v = maxveldiff_maxfromduplicates(points, num_points, &vcompare); // grab max from end

    /* Open output file */ 
    FILE* foutput = safefopen("task1.csv", "w");

    /* Print to file */ 
    fprintf(foutput, "x,y,u,v\n"); 
    fprintf(foutput, "%.6f,%.6f,%.6f,%.6f\n", max_u.x, max_u.y, max_u.u, max_u.v); 
    fprintf(foutput, "%.6f,%.6f,%.6f,%.6f\n", min_u.x, min_u.y, min_u.u, min_u.v); 
    fprintf(foutput, "%.6f,%.6f,%.6f,%.6f\n", max_v.x, max_v.y, max_v.u, max_v.v); 
    fprintf(foutput, "%.6f,%.6f,%.6f,%.6f\n", min_v.x, min_v.y, min_v.u, min_v.v); 

    /* Close output file */
    fclose(foutput);
}

void coarsegrid(point_t* points, int num_points, int resolution) {

    /* Create 2D array that represents the coarse grid */
    gridcell_t* grid = safemalloc(resolution * resolution * sizeof(gridcell_t)); 

    for (int i = 0; i < resolution; i++) {

        for (int j = 0; j < resolution; j++) {

            int grid_index = i * resolution + j; 

            grid[grid_index].x_average = 0; 
            grid[grid_index].y_average = 0; 
            grid[grid_index].u_average = 0; 
            grid[grid_index].v_average = 0; 
            grid[grid_index].points_in_cell_length = COARSE_GRID_INIT_POINTS_IN_CELL_LENGTH;
            grid[grid_index].points_in_cell = safemalloc(grid[grid_index].points_in_cell_length * sizeof(point_t));
            grid[grid_index].num_points_in_cell = 0;
        }
    }

    /* Add points to appropriate cell in the grid */ 
    for (int n = 0; n < num_points; n++) {

        point_t point = points[n];

        /* Make sure point is within the grid bounds */ 
        if (point.x < COARSE_GRID_MIN_X || point.x > COARSE_GRID_MAX_X ||
            point.y < COARSE_GRID_MIN_Y || point.y > COARSE_GRID_MAX_Y) {
            // outside bounds, skip this point
            continue; 
        }

        /* add the point to the correct grid cell(s) */ 
        addpointtogrid(grid, point, resolution);
    }

    /* Compute averages and score for each grid cell */ 
    for (int i = 0; i < resolution; i++) {
        for (int j = 0; j < resolution; j++) {

            int grid_index = i * resolution + j; 

            float x_sum = 0.0f, y_sum = 0.0f, u_sum = 0.0f, v_sum = 0.0f;

            // check if there are points in the cell
            if (grid[grid_index].num_points_in_cell <= 0) {

                grid[grid_index].x_average = 0.0f; 
                grid[grid_index].y_average = 0.0f;
                grid[grid_index].u_average = 0.0f;
                grid[grid_index].v_average = 0.0f;
                grid[grid_index].score = 0.0f;

            } else {

                // sum up the x, y, u, v values for the points in the cell
                for (int n = 0; n < grid[grid_index].num_points_in_cell; n++) {

                    point_t point = grid[grid_index].points_in_cell[n];

                    x_sum += point.x; 
                    y_sum += point.y; 
                    u_sum += point.u; 
                    v_sum += point.v; 
                }

                // compute the average from points
                grid[grid_index].x_average = x_sum / grid[grid_index].num_points_in_cell; 
                grid[grid_index].y_average = y_sum / grid[grid_index].num_points_in_cell; 
                grid[grid_index].u_average = u_sum / grid[grid_index].num_points_in_cell; 
                grid[grid_index].v_average = v_sum / grid[grid_index].num_points_in_cell; 

                // compute the score
                grid[grid_index].score = 100 * 
                    sqrt(pow(grid[grid_index].u_average, 2) + pow(grid[grid_index].v_average, 2)) / 
                    sqrt(pow(grid[grid_index].x_average, 2) + pow(grid[grid_index].y_average, 2));
            }

            /* No longer need to keep the complete list of points in each cell, just the average, 
               so free memory */ 
            free(grid[grid_index].points_in_cell);
            grid[grid_index].points_in_cell = NULL;
        }
    }

    /* Sort grid cells by descending score */
    quicksortgridcells(grid, 0, resolution * resolution - 1, &scorecompare);

    /* Open output file */ 
    FILE* foutput = safefopen("task2.csv", "w");
    fprintf(foutput, "x,y,u,v,S\n");

    /* Print coarse grid to file */ 
    for (int n = 0; n < resolution * resolution; n++) {
        fprintf(foutput, "%.6f,%.6f,%.6f,%.6f,%.6f\n", grid[n].x_average,
            grid[n].y_average, grid[n].u_average, grid[n].v_average, 
            grid[n].score);
    }

    /* Close output file */ 
    fclose(foutput);

    /* Free memory */ 
    free(grid); 
}

void velstat(point_t* points, int num_points) {

    /* Open output file */ 
    FILE* foutput = safefopen("task3.csv", "w");
    fprintf(foutput, "threshold,points,percentage\n");

    /* Sort points by ascending u velocity */ 
    quicksortpoints(points, 0, num_points-1, &ucompare);

    /* Calculate stats */
    int count = 0; 
    float percentage = 0.0f; 

    for (int threshold = VEL_STAT_MIN_THRESHOLD; threshold <= VEL_STAT_MAX_THRESHOLD; threshold++) {

        // Count the number of points up to each threshold
        while (count < num_points && fabsf(points[count].u) < threshold/VEL_STAT_THRESHOLD_DIVIDER) {
            count++; 
        }

        // Compute percentage
        percentage = ((float) count) * FRACTION_TO_PERCENT / num_points; 

        // Print results
        fprintf(foutput, "%.6f,%d,%.6f\n", threshold/VEL_STAT_THRESHOLD_DIVIDER, count, percentage);
    }

    /* Close output file */
    fclose(foutput);
}

void wakevis(point_t* points, int num_points) {

    /* First sort the points by ascending x coordinate */ 
    quicksortpoints(points, 0, num_points-1, &xcompare);

    /* Allocate array that stores the points with maximum u in each x bound */ 
    point_t* wakevis_points = safemalloc((round((WAKE_VIS_MAX_X - WAKE_VIS_MIN_X)
       / ((float) WAKE_VIS_X_INCREMENT)) + 1) * sizeof(point_t));
    
    /* Find where each x bound resides in the points array, and sort these points to find max u */
    int index = 0, num_wakevis_points = 0; 
    for (int x = WAKE_VIS_MIN_X; x <= WAKE_VIS_MAX_X; x += WAKE_VIS_X_INCREMENT) {

        // find index of the first point in the x bound
        while (index < num_points-1 && points[index].x < x - WAKE_VIS_BOUND) {
            index++;
        } 
        int x_lower_index = index; 

        // find index of the last point in the x bound
        while (index < num_points-1 && points[index].x < x + WAKE_VIS_BOUND) {
            index++;
        }
        int x_upper_index = index - 1; 

        if (x_upper_index >= x_lower_index) {

            // sort points in x bound by ascending u coordinate
            quicksortpoints(points, x_lower_index, x_upper_index, &ucompare);

            // get the point in x bound with maximum u coordinate and store it
            wakevis_points[num_wakevis_points] = wakevis_pointfromduplicates(points, x_lower_index, x_upper_index, &ucompare);
            num_wakevis_points++;
        } 
    } 

    /* Open output file */ 
    FILE* foutput = safefopen("task4_1.csv", "w");
    fprintf(foutput, "x,y_h\n");

    /* Print the wake vis points to file */ 
    for (int i = 0; i < num_wakevis_points; i++) {
        fprintf(foutput, "%6f,%.6f\n", wakevis_points[i].x, fabsf(wakevis_points[i].y));
    }

    /* Close output file */
    fclose(foutput);

    /* Populate spacing array */ 
    int i,j;
    int n = 12; // Location in x for wake visualization
    float* yheight;
    yheight = (float*) calloc(n,sizeof(float));

    /* Calculate spacing and output into the array yheight */ 
    if (num_wakevis_points < n) {
        // less points than required for wake vis
        printf("ERROR: not enough points, wake visualization incomplete.");
        exit(EXIT_FAILURE);
    } else {
        for (int i = 0; i < n; i++) {
            yheight[i] = ceil(10 * fabsf(wakevis_points[i].y));
        }
    }    

    /* Free memory */
    free(wakevis_points);

    /* Task 4: Part 2, nothing is to be changed here
       Remember to output the spacing into the array yheight
       for this to work. You also need to initialize i,j and 
       yheight so the skeleton as it stands will not compile */
    
    FILE *ft42;
    ft42 = safefopen("task4_2.txt","w");
    for (j = 11; j>=0; j--){
	for (i=0;i<yheight[j]-yheight[0]+4;i++){
 	    fprintf(ft42, " ");
	}
    	fprintf(ft42, "*\n");
    }
    for (i=0;i<5; i++){
    	fprintf(ft42, "III\n");
    }
    for(j = 0; j<12; j++ ){
    	for (i=0;i<yheight[j]-yheight[0]+4;i++){
    	    fprintf(ft42, " ");
    	}
    	fprintf(ft42, "*\n");
    }
    fclose(ft42);
    
    /* Cleanup */
    free(yheight);
}

/* 
 * Function: xcompare
 * ------------------
 * Compares the x coordinates of two flow points. 
 *
 * a: one flow point
 * b: the other flow point
 * 
 * returns: returns 0 if a.x and b.x equal,
 *          returns -1 if a.x < b.x,
 *          returns 1 if a.x > b.x
 */ 
int xcompare(point_t a, point_t b) {
    if (a.x < b.x) {
        return -1; 
    } else if (a.x > b.x) {
        return 1; 
    } else {
        return 0; 
    }
}

/* 
 * Function: ycompare
 * ------------------
 * Compares the y coordinates of two flow points. 
 *
 * a: one flow point
 * b: the other flow point
 * 
 * returns: returns 0 if a.y and b.y equal,
 *          returns -1 if a.y < b.y,
 *          returns 1 if a.y > b.y
 */ 
int ycompare(point_t a, point_t b) {
    if (a.y < b.y) {
        return -1; 
    } else if (a.y > b.y) {
        return 1; 
    } else {
        return 0; 
    }
}

/* 
 * Function: ucompare
 * ------------------
 * Compares the u coordinates of two flow points. 
 *
 * a: one flow point
 * b: the other flow point
 * 
 * returns: returns 0 if a.u and b.u equal,
 *          returns -1 if a.u < b.u,
 *          returns 1 if a.u > b.u
 */ 
int ucompare(point_t a, point_t b) {
    if (a.u < b.u) {
        return -1; 
    } else if (a.u > b.u) {
        return 1; 
    } else {
        return 0; 
    }
}

/* 
 * Function: vcompare
 * ------------------
 * Compares the v coordinates of two flow points. 
 *
 * a: one flow point
 * b: the other flow point
 * 
 * returns: returns 0 if a.v and b.v equal,
 *          returns -1 if a.v < b.v,
 *          returns 1 if a.v > b.v
 */ 
int vcompare(point_t a, point_t b) {
    if (a.v < b.v) {
        return -1; 
    } else if (a.v > b.v) {
        return 1; 
    } else {
        return 0; 
    }
}

/* 
 * Function: scorecompare
 * ----------------------
 * Compares the score coordinates of two coarse grid gridcells. 
 *
 * a: one gridcell
 * b: the other gridcell
 * 
 * returns: returns 0 if a.score and b.score equal,
 *          returns -1 if a.score < b.score,
 *          returns 1 if a.score > b.score
 */ 
int scorecompare(gridcell_t a, gridcell_t b) {
    if (a.score < b.score) {
        return -1; 
    } else if (a.score > b.score) {
        return 1; 
    } else {
        return 0; 
    }
}

/* 
 * Function: partitionpoints
 * -------------------------
 * Helper function that partitions an array of points about a pivot element 
 * for a quicksort algorithm
 * 
 * points: the array of points
 * pivot: the point to pivot about 
 * low: the index of the first element in section to partition
 * high: the index of the last element in section to partition
 * compare: the comparison function used to compare points
 * 
 * returns: returns the index of the partition point
 */ 
int partitionpoints(point_t* points, point_t pivot, int low, int high, int (*compare)(point_t, point_t)) {
    int i = low - 1; 

    for (int j = low; j < high; j++) {
        if (compare(points[j], pivot) <= 0) {
            i++; 
            swappoints(&points[i], &points[j]); 
        }
    }

    swappoints(&points[i + 1], &points[high]); 
    return (i + 1); 
}

/* 
 * Function: quicksortpoints
 * -------------------------
 * Quicksort algorithm that sorts an array of flow points according to a given 
 * comparison function.
 * 
 * points: the array of points
 * low: the index of the first element in section being sorted
 * high: the index of the last element in section being sorted
 * compare: the comparison function used to compare points
 * 
 * returns: void
 */ 
void quicksortpoints(point_t* points, int low, int high, int (*compare)(point_t, point_t)) { 
    if (low < high) {
        point_t pivot = points[high]; 
        int partition_index = partitionpoints(points, pivot, low, high, compare); 
        quicksortpoints(points, low, partition_index-1, compare);
        quicksortpoints(points, partition_index + 1, high, compare); 
    }
}

/* 
 * Function: partitiongridcells
 * ----------------------------
 * Helper function that partitions an array of gridcells about a pivot element 
 * for a quicksort algorithm
 * 
 * gridcells: the array of gridcells
 * pivot: the point to pivot about 
 * low: the index of the first element in section to partition
 * high: the index of the last element in section to partition
 * compare: the comparison function used to compare gridcells
 * 
 * returns: returns the index of the partition point
 */ 
int partitiongridcells(gridcell_t* gridcells, gridcell_t pivot, int low, int high, int (*compare)(gridcell_t, gridcell_t)) {
    int i = low - 1; 

    for (int j = low; j < high; j++) {
        if (compare(gridcells[j], pivot) >= 0) {
            i++; 
            swapgridcells(&gridcells[i], &gridcells[j]); 
        }
    }

    swapgridcells(&gridcells[i + 1], &gridcells[high]); 
    return (i + 1); 
}

/* 
 * Function: quicksortgridcells
 * -------------------------
 * Quicksort algorithm that sorts an array of coarse grid gridcells according to a given 
 * comparison function.
 * 
 * points: the array of gridcells
 * low: the index of the first element in section being sorted
 * high: the index of the last element in section being sorted
 * compare: the comparison function used to compare gridcells
 * 
 * returns: void
 */ 
void quicksortgridcells(gridcell_t* gridcells, int low, int high, int (*compare)(gridcell_t, gridcell_t)) {
    if (low < high) {
        gridcell_t pivot = gridcells[high]; 
        int partition_index = partitiongridcells(gridcells, pivot, low, high, compare); 
        quicksortgridcells(gridcells, low, partition_index-1, compare);
        quicksortgridcells(gridcells, partition_index + 1, high, compare); 
    }
}

/* 
 * Function: swappoints
 * --------------------
 * Swaps the values of two points 
 * 
 * a: one point
 * b: the other point
 * 
 * returns: void
 */ 
void swappoints(point_t* a, point_t* b) {
    point_t temp = *a; 
    *a = *b; 
    *b = temp; 
}

/* 
 * Function: swappoints
 * --------------------
 * Swaps the values of two gridcells 
 * 
 * a: one gridcell
 * b: the other gridcell
 * 
 * returns: void
 */ 
void swapgridcells(gridcell_t* a, gridcell_t* b) {
    gridcell_t temp = *a;
    *a = *b;  
    *b = temp; 
}

/* 
 * Function: maxveldiff_minfromduplicates
 * --------------------------------------
 * When given a sorted array of points, checks the duplicate minimum values from the 
 * start of the array and picks the one with lowest x coordinate. If there are multiple
 * points with this same x coordinate, picks the point with the lowest y coordinate. 
 * 
 * points: a sorted array of points
 * num_points: the number of points stored in the points array
 * compare: the comparison function used to compare points
 * 
 * returns: the point from the duplicate minimum values with the lowest x coordinate 
 * (and lowest y coordinate to break further ties)
 */ 
point_t maxveldiff_minfromduplicates(point_t* points, int num_points, int (*compare)(point_t, point_t)) {
    int i = 1; 
    while (i < num_points && compare(points[i], points[i-1]) == 0) {
        i++; 
    }

    if (i > 1) {
        quicksortpoints(points, 0, i-1, &xcompare);
    } else {
        // no duplicates
        return points[0]; 
    }
    
    int j = 1;  
    while (j < i && xcompare(points[j], points[j-1]) == 0) {
        j++;
    }

    if (j > 1) {
        quicksortpoints(points, 0, j-1, &ycompare);
    } else {
        // no duplicates with same x value
        return points[0]; 
    }

    return points[0]; 
}

/* 
 * Function: maxveldiff_maxfromduplicates
 * --------------------------------------
 * When given a sorted array of points, checks the duplicate maximum values from the 
 * end of the array and picks the one with lowest x coordinate. If there are multiple
 * points with this same x coordinate, picks the point with the lowest y coordinate. 
 * 
 * points: a sorted array of points
 * num_points: the number of points stored in the points array
 * compare: the comparison function used to compare points
 * 
 * returns: the point from the duplicate maximum values with the lowest x coordinate 
 * (and lowest y coordinate to break further ties)
 */ 
point_t maxveldiff_maxfromduplicates(point_t* points, int num_points, int (*compare)(point_t, point_t)) {
    int i = num_points-1; 
    while (i > 0 && compare(points[i], points[i-1]) == 0) {
        i--; 
    }

    if (i < num_points-1) {
        quicksortpoints(points, i, num_points-1, &xcompare);
    } else {
        // no duplicates
        return points[i]; 
    }

    int j = i+1; 
    while (j < num_points && xcompare(points[j], points[j-1]) == 0) {
        j++;
    }

    if (j > i + 1) {
        quicksortpoints(points, i, j-1, &ycompare);
    } else {
        // no duplicates with same x value
        return points[i]; 
    }

    return points[i];
}

/* 
 * Function: wakevis_pointfromduplicates
 * -------------------------------------
 * When given a sorted array of points, checks the duplicate maximum values from the 
 * end of the array and picks the one with lowest y coordinate. 
 * 
 * points: a sorted array of points
 * num_points: the number of points stored in the points array
 * compare: the comparison function used to compare points
 * 
 * returns: the point from the duplicate maximum values with the lowest y coordinate 
 */ 
point_t wakevis_pointfromduplicates(point_t* points, int lower_index, int upper_index, int (*compare)(point_t, point_t)) {
    int i = upper_index; 
    while (i > lower_index && compare(points[i], points[i-1]) == 0) {
        i--; 
    }

    if (i < upper_index) {
        quicksortpoints(points, i, upper_index, &ycompare);
    } else {
        // no duplicates
        return points[i]; 
    }

    return points[i];
}

/* 
 * Function: addpointtogrid
 * ------------------------
 * Adds a flow point to the coarse grid. Adds the point to all grid cells that point
 * is in or on the boundary of. 
 * 
 * grid: the array of gridcells that defines the grid
 * point: the point being added to the grid
 * resolution: the number of grid cells in the x and y directions 
 * 
 * returns: void
 */ 
void addpointtogrid(gridcell_t* grid, point_t point, int resolution) {

    // compute the size of a grid cell in the x and y directions
    float x_cell_size = (COARSE_GRID_MAX_X - COARSE_GRID_MIN_X) / (float) resolution; 
    float y_cell_size = (COARSE_GRID_MAX_Y - COARSE_GRID_MIN_Y) / (float) resolution; 

    // iterate through the cells along x
    for (int i = 0; i < resolution; i++) {

        // compute the lower and upper bounds in the x direction of each grid cell
        float x_lower_bound = COARSE_GRID_MIN_X + i * x_cell_size;
        float x_upper_bound = COARSE_GRID_MIN_X + (i + 1) * x_cell_size;

        if (point.x >= x_lower_bound && point.x <= x_upper_bound) {

            // iterate through the cells along y
            for (int j = 0; j < resolution; j++) {

                // compute the lower and upper bounds in the y direction of each grid cell
                float y_lower_bound = COARSE_GRID_MIN_Y + j * y_cell_size;
                float y_upper_bound = COARSE_GRID_MIN_Y + (j + 1) * y_cell_size;

                // check if point is in this grid cell
                if (point.y >= y_lower_bound && point.y <= y_upper_bound) {
                    
                    int grid_index = i * resolution + j; 

                    // reallocate memory if required
                    if (grid[grid_index].num_points_in_cell >= grid[grid_index].points_in_cell_length) {
                        grid[grid_index].points_in_cell_length *= 2; // double the size of the array

                        grid[grid_index].points_in_cell = saferealloc(grid[grid_index].points_in_cell, 
                            grid[grid_index].points_in_cell_length * sizeof(point_t));
                    }

                    // add point to cell
                    grid[grid_index].points_in_cell[grid[grid_index].num_points_in_cell] = point; 
                    grid[grid_index].num_points_in_cell++; 
                }
            }
        }
    }
}

/* 
 * Function: safemalloc
 * --------------------
 * Safe malloc function that checks the memory allocation was successful before returning 
 * the pointer to the memory.
 * 
 * size: the size of memory required
 * 
 * returns: the pointer to the memory allocation
 */ 
void* safemalloc(size_t size) {
    void* ptr= NULL;

    ptr = malloc(size); 

    if (ptr == NULL) {
        printf("ERROR: %ld bytes could not be allocated.", size);
        exit(EXIT_FAILURE); 
    }

    return ptr; 
}

/* 
 * Function: saferealloc
 * ---------------------
 * Safe realloc function that checks the memory reallocation was successful before returning 
 * the pointer to the memory.
 * 
 * ptr: the pointer to the currently allocated memory
 * size: the size of memory required
 * 
 * returns: the pointer to the memory allocation
 */ 
void* saferealloc(void* ptr, size_t size) {

    ptr = realloc(ptr, size); 

    if (ptr == NULL) {
        printf("ERROR: %ld bytes could not be reallocated.", size);
        exit(EXIT_FAILURE); 
    }

    return ptr; 
}

/* 
 * Function: safefopen
 * -------------------
 * Safe fopen function that checks the opening of the file was successful before returning
 * the pointer to the file.
 * 
 * filename: the name of the file
 * mode: the mode to open the file with ("r" read, "w" write)
 * 
 * returns: the pointer to the file 
 */ 
FILE* safefopen(const char* filename, const char* mode) {
    FILE* file = NULL; 

    file = fopen(filename, mode);

    if (file == NULL) {
        printf("ERROR: file could not be opened.");
        exit(EXIT_FAILURE); 
    }

    return file; 
}


