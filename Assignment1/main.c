/***************************************************************************
 *
 *   File        : main.c
 *   Student Id  : 638697
 *   Name        : Simeon Bain
 *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>
#include "tasks.h"

#define NUM_COMMAND_LINE_ARGUMENTS 3
#define FLOW_FILE_COMMAND_LINE_ARGUMENT 1
#define RESOLUTION_COMMAND_LINE_ARGUMENT 2
#define INITIAL_POINTS_ARRAY_LENGTH 26000
#define BUFFER_LENGTH 50
#define SEC_TO_MILLISEC 1000

int main(int argc, char *argv[]) {
	
	/* Parse Command Line Arguments */
	if (argc < NUM_COMMAND_LINE_ARGUMENTS) {
		printf("ERROR: Not enough command line arguments.\n");
		exit(EXIT_FAILURE);
	}
	
	char* flow_file = argv[FLOW_FILE_COMMAND_LINE_ARGUMENT];
	int resolution = atoi(argv[RESOLUTION_COMMAND_LINE_ARGUMENT]);

	/* Open data file */ 
    FILE* fdata = safefopen(flow_file, "r");

    /* Create array to store flow data */ 
    int points_length = INITIAL_POINTS_ARRAY_LENGTH;
    point_t* points = safemalloc(points_length * sizeof(point_t)); 

    /* Read in flow data from data file */ 
    char* buffer = safemalloc(BUFFER_LENGTH * sizeof(char));
    char* token; 
    int num_points = 0; 
    point_t curr_point; 

    fgets(buffer, BUFFER_LENGTH, fdata); // consume the first line

    while(fgets(buffer, BUFFER_LENGTH, fdata)) {

        token = strtok(buffer, ","); 
        curr_point.x = atof(token);
        token = strtok(NULL, ",");
        curr_point.y = atof(token);
        token = strtok(NULL, ",");
        curr_point.u = atof(token);
        token = strtok(NULL, ",");
        curr_point.v = atof(token); 

        // reallocate memory if required 
        if (num_points >= points_length) { 
            points_length *= 2; // double the size of the array
            points = saferealloc(points, points_length * sizeof(point_t)); 
        }

        points[num_points] = curr_point;
        num_points++; 
    }

    /* Close flow data file, free memory */
    fclose(fdata); 
    free(buffer);

	/* Set up timing to print running time for each task in ms */ 
    clock_t start;
    clock_t end; 
    float ms_elapsed = 0.0f; 

	/* Task 1: Find the maximum velocity difference */
	start = clock(); 
	maxveldiff(points, num_points);
	end = clock();
	ms_elapsed = (float) (end - start) * SEC_TO_MILLISEC / CLOCKS_PER_SEC;

	printf("TASK 1: %.2f milliseconds\n", ms_elapsed);
	
	/* Task 2: Coarser Grid */
	start = clock(); 
	coarsegrid(points, num_points, resolution);
	end = clock();
	ms_elapsed = (float) (end - start) * SEC_TO_MILLISEC / CLOCKS_PER_SEC;	

	printf("TASK 2: %.2f milliseconds\n", ms_elapsed);
	
	/* Task 3: Statistics */
	start = clock(); 
	velstat(points, num_points);
	end = clock();
	ms_elapsed = (float) (end - start) * SEC_TO_MILLISEC / CLOCKS_PER_SEC;

	printf("TASK 3: %.2f milliseconds\n", ms_elapsed);
	
	/* Task 4: Wake height and visualisation */
	start = clock(); 
	wakevis(points, num_points);
	end = clock();
	ms_elapsed = (float) (end - start) * SEC_TO_MILLISEC / CLOCKS_PER_SEC;

	printf("TASK 4: %.2f milliseconds\n", ms_elapsed);

	/* Free memory */
	free(points);
    
	return (EXIT_SUCCESS);
}
