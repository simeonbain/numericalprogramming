/***************************************************************************
 *
 *   File        : tasks.h
 *   Student Id  : 638697
 *   Name        : Simeon Bain
 *
 ***************************************************************************/

#ifndef TASKS_H

/* Stores the position and velocity data of a point in the domain */ 
typedef struct point {
    float x; 
    float y; 
    float u; 
    float v; 
} point_t;

/* Represents a grid cell in the coarse grid. Stores the flow points that are   
 * within the cell, as well as the averages and score of those points.
 */
typedef struct gridcell {
	float x_average; 
	float y_average; 
	float u_average; 
	float v_average; 
	float score; 
	point_t* points_in_cell; 
	int points_in_cell_length;
	int num_points_in_cell;
} gridcell_t;


/* Task functions */ 
void maxveldiff(point_t* points, int num_points);

void coarsegrid(point_t* points, int num_points, int resolution);

void velstat(point_t* points, int num_points);

void wakevis(point_t* points, int num_points);


/* Comparison functions */ 
int xcompare(point_t a, point_t b);

int ycompare(point_t a, point_t b);

int ucompare(point_t a, point_t b);

int vcompare(point_t a, point_t b);

int scorecompare(gridcell_t a, gridcell_t b);


/* Sorting functions */ 
int partition(point_t* points, 
	point_t pivot, int low, int high, int (*compare)(point_t, point_t));

void quicksortpoints(
	point_t* points, int low, int high, int (*compare)(point_t, point_t));

int partitiongridcells(gridcell_t* gridcells, 
	gridcell_t pivot, int low, int high, int (*compare)(gridcell_t, gridcell_t));

void quicksortgridcells(
	gridcell_t* grid, int low, int high, int (*compare)(gridcell_t, gridcell_t)); 


/* Helper functions */ 
void swappoints(point_t* a, point_t* b);

void swapgridcells(gridcell_t* a, gridcell_t* b);

point_t maxveldiff_minfromduplicates(
	point_t* points, int num_points, int (*compare)(point_t, point_t));

point_t maxveldiff_maxfromduplicates(
	point_t* points, int num_points, int (*compare)(point_t, point_t));

point_t wakevis_pointfromduplicates(point_t* points, 
	int lower_index, int upper_index, int (*compare)(point_t, point_t));

void addpointtogrid(gridcell_t* grid, point_t point, int resolution);

void* safemalloc(size_t size); 

void* saferealloc(void* ptr, size_t size); 

FILE* safefopen(const char* filename, const char* mode);

#endif
