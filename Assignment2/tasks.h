/***************************************************************************
 *
 *   File        : tasks.h
 *   Student Id  : 638697
 *   Name        : Simeon Bain
 *
 ***************************************************************************/

#ifndef TASKS_H

void shockwave(const char* q2_file);

void linalgbsys(const char* q4_file);

void interp(const char* q5_file, const double xo);

void heateqn(const char* q6_file);

double shockwave_function(double M, double theta, double beta, double gamma); 

double shockwave_derivative(double M, double theta, double beta, double gamma); 

double shockwave_findroot(double M, double theta, double beta_initial, double gamma);

void linalgbsys_thomassolve(double* a, double* b, double* c, double* q, double* x, int dimension); 

double interp_lagrange(double* x, double* f, int num_points, double xo);

double interp_cubicspline(double* x, double* f, int num_points, double xo);

void heateqn_integrate_explicit_fe(double* f_n, double* f_nplus1, double mu, int Nx, double delta_t); 

void heateqn_integrate_explicit_ve(double* f_n, double* f_nplus1, double mu, int Nx, double delta_t); 

void heateqn_integrate_implicit_fe(double* f_n, double* f_nplus1, double mu, int Nx, double delta_t); 

double heateqn_RHS(double f_i, double f_iplus1, double f_iminus1, double delta_x, double mu); 

void* safemalloc(size_t size); 

void* saferealloc(void* ptr, size_t size); 

FILE* safefopen(const char* filename, const char* mode);

double degtorad(double angle); 

double radtodeg(double angle); 

double tan(double x);

double cot(double x); 

double cosec(double x); 

#endif
