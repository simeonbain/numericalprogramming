/***************************************************************************
 *
 *   File        : main.c
 *   Student Id  : 638697
 *   Name        : Simeon Bain
 *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <string.h>
#include "tasks.h"

#define NUM_COMMAND_LINE_ARGUMENTS 5
#define Q2_FILE_COMMAND_LINE_ARGUMENT 1
#define Q4_FILE_COMMAND_LINE_ARGUMENT 2
#define Q5_FILE_COMMAND_LINE_ARGUMENT 3
#define XO_COMMAND_LINE_ARGUMENT 4
#define Q6_COMMAND_LINE_ARGUMENT 5

int main(int argc, char *argv[]) {
	
	/* Parse Command Line Arguments */
	if (argc < NUM_COMMAND_LINE_ARGUMENTS) {
		printf("ERROR: Not enough command line arguments.\n");
		exit(EXIT_FAILURE);
	}

	char* q2_file = argv[Q2_FILE_COMMAND_LINE_ARGUMENT];
	char* q4_file = argv[Q4_FILE_COMMAND_LINE_ARGUMENT];
	char* q5_file = argv[Q5_FILE_COMMAND_LINE_ARGUMENT];
	double xo = atof(argv[XO_COMMAND_LINE_ARGUMENT]);
	char* q6_file = argv[Q6_COMMAND_LINE_ARGUMENT];
    
	/* Question 2 */
	shockwave(q2_file);
	
	/* Question 4 */
	linalgbsys(q4_file);
	
	/* Question 5 */
	interp(q5_file,xo);
	
	/* Question 6 */
	heateqn(q6_file);
    
	return (EXIT_SUCCESS);
}
