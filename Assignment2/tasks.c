/***************************************************************************
 *
 *   File        : tasks.c
 *   Student Id  : 638697
 *   Name        : Simeon Bain
 *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <string.h>
#include "tasks.h"

/* Shockwave Constants */ 
#define EPSILON 0.000001
#define MAX_ITERATIONS 1000
#define SHOCKWAVE_INITIAL_ARRAY_LENGTH 6
#define N_THETA 90
#define THETA_LOWER 0
#define THETA_UPPER 90
#define BETA_LOWER 0
#define BETA_UPPER 90

/* Linear Algebraic System Constants */
#define LINALGBSYS_INITIAL_ARRAY_LENGTH 5

/* Interpolation Constants */ 
#define INTERP_INITIAL_ARRAY_LENGTH 4
#define LAGRANGE_ORDER 2

/* Heat Equation Constants */ 
#define X_LOWER 0
#define X_UPPER 1
#define T_LOWER 0
#define T_UPPER 2
#define OUTPUT_TIMESTEP 100
#define LOWER_BOUNDARY_CONDITION 0.0
#define UPPER_BOUNDARY_CONDITION 0.0

/* Other Constants */ 
#define BUFFER_LENGTH 30
#define PI 3.14159265359

void shockwave(const char* q2_file) {
    
    /* Open files */ 
    FILE* finput = safefopen(q2_file, "r");
    FILE* foutput = safefopen("out_shock.csv", "w");

    /* Read in parameters from input file */ 
    char * buffer = safemalloc(BUFFER_LENGTH * sizeof(double)); 
    char* token; 

    // Read in part a) parameters
    fgets(buffer, BUFFER_LENGTH, finput); // consume the first line

    fgets(buffer, BUFFER_LENGTH, finput); // get the second line 
    token = strtok(buffer, ","); 
    double M_a = atof(token); 
    token = strtok(NULL, ",");
    double theta_a = atof(token); 
    token = strtok(NULL, ","); 
    double beta_l_initial = atof(token); 
    token = strtok(NULL, ","); 
    double beta_u_initial = atof(token); 
    token = strtok(NULL, ","); 
    double gamma = atof(token); 

    // Convert angles to radians
    theta_a = degtorad(theta_a);
    beta_l_initial = degtorad(beta_l_initial);
    beta_u_initial = degtorad(beta_u_initial); 

    // Read in part c) parameters
    fgets(buffer, BUFFER_LENGTH, finput); // consume a line

    int M_length = SHOCKWAVE_INITIAL_ARRAY_LENGTH; 
    double* M = safemalloc(M_length * sizeof(double)); 
    int num_M = 0; 

    while (fgets(buffer, BUFFER_LENGTH, finput)) {

        // reallocate memory if required 
        if (num_M > M_length) {
            M_length *= 2; // double the size of the array
            M = saferealloc(M, M_length * sizeof(double));
        }

        M[num_M] = atof(buffer); 

        num_M++; 
    }

    /* Close input file, free memory */ 
    fclose(finput); 
    free(buffer); 

    /* Part a) */ 
    // Find beta_l
    double beta_l = shockwave_findroot(M_a, theta_a, beta_l_initial, gamma);

    // Find beta_u
    double beta_u = shockwave_findroot(M_a, theta_a, beta_u_initial, gamma); 

    // Convert angles to degrees
    beta_l = radtodeg(beta_l);
    beta_u = radtodeg(beta_u);
    printf("%.4f, %.4f\n", beta_l, beta_u); // print the part a) result to command line

    /* Part b) */ 
    double delta_theta = (THETA_UPPER - THETA_LOWER) / (double) N_THETA;  

    for (int i = 0; i < N_THETA; i++) {
        double theta_b = THETA_LOWER + delta_theta * i; // iterate through theta
        theta_b = degtorad(theta_b); // convert to radians

        // Find beta_l
        beta_l = shockwave_findroot(M_a, theta_b, beta_l_initial, gamma); 

        // Find beta_u
        beta_u = shockwave_findroot(M_a, theta_b, beta_u_initial, gamma); 

        if (beta_u <= beta_l) {
            // we have gone past theta_max
            break;
        } else if (beta_l < BETA_LOWER || beta_l > BETA_UPPER ||
            beta_u < BETA_LOWER || beta_u > BETA_UPPER) {
            // the solution is no longer physical 
            break; 
        }

        // Convert angles to degrees
        theta_b = radtodeg(theta_b); 
        beta_l = radtodeg(beta_l);
        beta_u = radtodeg(beta_u);
    }

    /* Part c) */ 
    // Print title to output file
    fprintf(foutput, "M,theta,beta_lower,beta_upper\n");

    for (int i = 0; i < num_M; i++) {

        int theta_n = 0; 
        beta_l = beta_l_initial; 
        beta_u = beta_u_initial; 

        while (theta_n < THETA_UPPER) {

            double theta = degtorad(theta_n); // get theta in radians

            // Find beta_l
            beta_l = shockwave_findroot(M[i], theta, beta_l_initial, gamma); 

            // Find beta_u
            beta_u = shockwave_findroot(M[i], theta, beta_u_initial, gamma); 

            if ((beta_u < beta_l) || (degtorad(BETA_LOWER) > beta_l) || (beta_u > degtorad(BETA_UPPER))) {
                break; 
            }

            // Print results to output file
            fprintf(foutput, "%.4f,%d,%.4f,%.4f\n", M[i], theta_n, radtodeg(beta_l), radtodeg(beta_u));

            theta_n++; 
        }
    }

    /* Close output file */ 
    fclose(foutput);

    /* Free memory */ 
    free(M); 
}

void linalgbsys(const char* q4_file) {

    /* Open files */ 
    FILE* finput = safefopen(q4_file, "r");
    FILE* foutput = safefopen("out_linalsys.csv", "w");

    /* Creat arrays to store vector elements */ 
    int elements_length = LINALGBSYS_INITIAL_ARRAY_LENGTH; 
    double* a = safemalloc(elements_length * sizeof(double)); 
    double* b = safemalloc(elements_length * sizeof(double)); 
    double* c = safemalloc(elements_length * sizeof(double)); 
    double* q = safemalloc(elements_length * sizeof(double)); 

    /* Read in elements from input file */ 
    char* buffer = safemalloc(BUFFER_LENGTH * sizeof(char)); 
    char* token; 
    int num_elements = 0; 

    fgets(buffer, BUFFER_LENGTH, finput); // consume the first line

    while (fgets(buffer, BUFFER_LENGTH, finput)) {

        // reallocate memory if required
        if (num_elements >= elements_length) {
            elements_length *= 2; // double the size of the arrays
            a = saferealloc(a, elements_length * sizeof(double)); 
            b = saferealloc(b, elements_length * sizeof(double)); 
            c = saferealloc(c, elements_length * sizeof(double));
            q = saferealloc(q, elements_length * sizeof(double));
        }

        token = strtok(buffer, ",");
        a[num_elements] = atof(token);  
        token = strtok(NULL, ",");
        b[num_elements] = atof(token);
        token = strtok(NULL, ",");
        c[num_elements] = atof(token);  
        token = strtok(NULL, ",");
        q[num_elements] = atof(token);

        num_elements++; 
    }

    /* Close input file, free memory */
    fclose(finput); 
    free(buffer);

    /* Solve the system with the Thomas algorithm */ 
    double* x = safemalloc(num_elements * sizeof(double)); // solution vector
    linalgbsys_thomassolve(a, b, c, q, x, num_elements); 

    /* Print to file */ 
    fprintf(foutput, "x\n");
    for (int i = 0; i < num_elements; i++) {
        fprintf(foutput, "%.4f\n", x[i]);
    }

    /* Close output file */ 
    fclose(foutput); 

    /* Free memory */ 
    free(a); 
    free(b);
    free(c); 
    free(q); 
    free(x); 
}

void interp(const char* q5_file, const double xo) {

	/* Open files */ 
	FILE* finput = safefopen(q5_file, "r");
    FILE* foutput = safefopen("out_interp.csv", "w");

    /* Create arrays to store points */
    int points_length = INTERP_INITIAL_ARRAY_LENGTH; 
    double* x = safemalloc(points_length * sizeof(double)); 
    double* f = safemalloc(points_length * sizeof(double)); 

    /* Read in points from input file */ 
    char* buffer = safemalloc(BUFFER_LENGTH * sizeof(char)); 
    char* token; 
    int num_points = 0; 

    fgets(buffer, BUFFER_LENGTH, finput); // consume the first line

    while (fgets(buffer, BUFFER_LENGTH, finput)) {

    	// reallocate memory if required
    	if (num_points >= points_length) {
    		points_length *= 2; // double the size of the arrays
    		x = saferealloc(x, points_length * sizeof(double)); 
    		f = saferealloc(f, points_length * sizeof(double)); 
    	}

    	token = strtok(buffer, ",");
    	x[num_points] = atof(token);  
    	token = strtok(NULL, ",");
    	f[num_points] = atof(token); 

    	num_points++; 
    }

    /* Close input file, free memory */
    fclose(finput); 
    free(buffer);

	/* Interpolate using Lagrange polynomial */
    double* x_lagrange = safemalloc((LAGRANGE_ORDER + 1) * sizeof(double));
    double* f_lagrange = safemalloc((LAGRANGE_ORDER + 1) * sizeof(double));

    // Make sure we have enough points
    if (num_points < LAGRANGE_ORDER + 1) {
    	printf("ERROR: not enough points to perform order %d Lagrange interpolation\n", LAGRANGE_ORDER); 
    	exit(EXIT_FAILURE);
    }

     // Select the correct number of points (from the end) for the order of Lagrange polynomial
    int j = num_points - LAGRANGE_ORDER - 1; 
    for (int i = 0; i <= LAGRANGE_ORDER; i++) {
    	x_lagrange[i] = x[j];
    	f_lagrange[i] = f[j]; 
    	j++; 
    }

    // Perform the interpolation
	double f_xo = interp_lagrange(x_lagrange, f_lagrange, LAGRANGE_ORDER + 1, xo); 

	/* Print to file */ 
    fprintf(foutput, "lagrange\n");
    fprintf(foutput, "%.4f\n", f_xo);

	/* Interpolate using cubic spline polynomial */ 
    // make sure we have enough points
    if (num_points <= 1) {
        printf("ERROR: not enough points to perform cubic spline interpolation\n"); 
        exit(EXIT_FAILURE);
    }

    // perform the interpolation
	f_xo = interp_cubicspline(x, f, num_points, xo);

	/* Free memory */ 
    free(x_lagrange);
    free(f_lagrange); 
	free(x);
	free(f);

	/* Print to file */ 
    fprintf(foutput, "cubic\n");
    fprintf(foutput, "%.4f\n", f_xo);

    /* Close output file */
    fclose(foutput);
}

void heateqn(const char* q6_file) {

    /* Open files */ 
    FILE* finput = safefopen(q6_file, "r");
    FILE* foutput_explicit_fe = safefopen("out_heateqn_explicit_fe.csv", "w"); 
    FILE* foutput_explicit_ve = safefopen("out_heateqn_explicit_ve.csv", "w");
    FILE* foutput_implicit_fe = safefopen("out_heateqn_implicit_fe.csv", "w"); 

    /* Read in parameters from input file */
    char* buffer = safemalloc(BUFFER_LENGTH * sizeof(char)); 
    char* token;

    fgets(buffer, BUFFER_LENGTH, finput); // consume the first line

    fgets(buffer, BUFFER_LENGTH, finput); // get the second line
    token = strtok(buffer, ",");
    double mu = atof(token);   
    token = strtok(NULL, ",");
    double Nx = atof(token); 
    token = strtok(NULL, ",");
    double Nt = atof(token); 

    /* Close input file, free memory */ 
    fclose(finput); 
    free(buffer); 

    /* Set up output files */ 
    fprintf(foutput_explicit_fe, "x,f(x)\n");
    fprintf(foutput_explicit_ve, "x,f(x)\n"); 
    fprintf(foutput_implicit_fe, "x,f(x)\n");  

    /* Create 2D arrays that store the x and t dimensions for each scheme */ 
    double** f_exp_fe = safemalloc((Nt + 1) * sizeof(double)); 
    double** f_exp_ve = safemalloc((Nt + 1) * sizeof(double)); 
    double** f_imp_fe = safemalloc((Nt + 1) * sizeof(double)); 

    for (int k = 0; k <= Nt; k++) {
        f_exp_fe[k] = safemalloc((Nx + 1) * sizeof(double));
        f_exp_ve[k] = safemalloc((Nx + 1) * sizeof(double));  
        f_imp_fe[k] = safemalloc((Nx + 1) * sizeof(double)); 
    }

    /* Initial condition */ 
    for (int i = 0; i <= Nx; i++) {
        double x = i * (X_UPPER - X_LOWER) / (double) Nx;
        
        if (0 <= x && x < 0.125) {
            f_exp_fe[0][i] = 0; 
            f_exp_ve[0][i] = 0; 
            f_imp_fe[0][i] = 0; 
        } else if (0.375 < x && x <= 1) {
            f_exp_fe[0][i] = 0; 
            f_exp_ve[0][i] = 0; 
            f_imp_fe[0][i] = 0; 
        } else {
            double f_init = 0.5 * (1 - cos(8 * PI * (x - 0.125)));
            f_exp_fe[0][i] = f_init; 
            f_exp_ve[0][i] = f_init;
            f_imp_fe[0][i] = f_init;
        }
    }

    /* Loop over the timesteps and call the time integration routine */ 
    double delta_t = (T_UPPER - T_LOWER) / (double) Nt; 

    for (int k = 1; k <= Nt; k++) {
        heateqn_integrate_explicit_fe(f_exp_fe[k-1], f_exp_fe[k], mu, Nx, delta_t);
        heateqn_integrate_explicit_ve(f_exp_ve[k-1], f_exp_ve[k], mu, Nx, delta_t);
        heateqn_integrate_implicit_fe(f_imp_fe[k-1], f_imp_fe[k], mu, Nx, delta_t);
    }

    /* Print the result at the output timestep */ 
    if (Nt >= OUTPUT_TIMESTEP) {
        for (int i = 0; i <= Nx; i++) {
            double x = i * (X_UPPER - X_LOWER) / (double) Nx;

            fprintf(foutput_explicit_fe, "%.4f, %.4f\n", x, f_exp_fe[OUTPUT_TIMESTEP][i]);
            fprintf(foutput_explicit_ve, "%.4f, %.4f\n", x, f_exp_ve[OUTPUT_TIMESTEP][i]);
            fprintf(foutput_implicit_fe, "%.4f, %.4f\n", x, f_imp_fe[OUTPUT_TIMESTEP][i]);
        }
    }

    /* Free memory */ 
    for (int k = 0; k <= Nt; k++) {
        free(f_exp_fe[k]); 
        free(f_exp_ve[k]); 
        free(f_imp_fe[k]); 
    }
    free(f_exp_fe); 
    free(f_exp_ve);
    free(f_imp_fe); 

    /* Close output files */ 
    fclose(foutput_explicit_fe);
    fclose(foutput_explicit_ve);
    fclose(foutput_implicit_fe); 
}

/* 
 * Function: shockwave_function
 * --------------------
 * Returns the value of the shockwave function f(M, theta, beta, gamma)
 */ 
double shockwave_function(double M, double theta, double beta, double gamma) {

    return 2 * cot(beta)  * (pow(M, 2) * pow(sin(beta), 2) - 1) / 
        (pow(M, 2) * (gamma + cos(2 * beta)) + 2) - tan(theta);
}

/* 
 * Function: shockwave_derivative
 * --------------------
 * Returns the value of the derivate of the shockwave function f(M, theta, beta, gamma)
 */ 
double shockwave_derivative(double M, double theta, double beta, double gamma) {

    return -(pow(cosec(beta), 2) * (pow(M, 4) * gamma * cos(4 * beta) - 2 * pow(M, 2) * cos(2 * beta) * 
        (pow(M, 2) * (gamma - 1) + 4) + pow(M, 4) * gamma - 2 * pow(M, 4) - 4 * pow(M, 2) * gamma + 4 * pow(M, 2) - 8)) / 
        (2 * pow(pow(M, 2) * cos(2 * beta) + pow(M, 2) * gamma + 2, 2));
}

/* 
 * Function: shockwave_findroot
 * --------------------
 * Finds the solution of the shockwave function given an initial guess beta_initial
 */ 
double shockwave_findroot(double M, double theta, double beta_initial, double gamma) {

    double f_i = shockwave_function(M, theta, beta_initial, gamma);
    double df_i = 0;
    double beta_i = beta_initial; 

    int num_iterations = 0; 
    while (num_iterations < MAX_ITERATIONS) {
        f_i = shockwave_function(M, theta, beta_i, gamma); 

        df_i = shockwave_derivative(M, theta, beta_i, gamma); 

        beta_i = beta_i - f_i / df_i; 

        if (fabs(f_i) < EPSILON) {
            break; 
        } 

        num_iterations++; 
    }

    return beta_i; 
}

/* 
 * Function: linalgbsys_thomassolve
 * --------------------
 * Solves a tri-diagonal linear algebraic system using the Thomas algorithm 
 */ 
void linalgbsys_thomassolve(double* a, double* b, double* c, double* q, double* x, int dimension) {

    // Allocate memory for a-star and q-star vectors
    double* astar = safemalloc(dimension * sizeof(double));
    double* qstar = safemalloc(dimension * sizeof(double));

    // Calculate a-star elements
    astar[0] = a[0]; 
    for (int i = 1; i < dimension; i++) {
        astar[i] = a[i] - c[i] * b[i-1] / astar[i-1];
    }

    // Calculate q-star elements
    qstar[0] = q[0]; 
    for (int i = 1; i < dimension; i++) {
        qstar[i] = q[i] - c[i] * qstar[i-1] / astar[i-1]; 
    }

    // Calculate the solution elements
    x[dimension-1] = qstar[dimension-1] / astar[dimension-1]; 
    for (int i = dimension - 2; i >= 0; i--) {
        x[i] = (qstar[i] - b[i] * x[i+1]) / astar[i]; 
    }

    // Free memory
    free(astar);
    free(qstar);
}

/* 
 * Function: interp_lagrange
 * --------------------
 * Returns the 2nd order Lagrange interpolation of the function f at the point xo
 */ 
double interp_lagrange(double* x, double* f, int num_points, double xo) {

    // Make sure we are interpolating (not extrapolating)
    if (xo < x[0] || x[0] > x[num_points-1]) {
        printf("ERROR: evaluation point is not within dataset\n");
        exit(EXIT_FAILURE);
    }

	// Find L function values 
	double* L = safemalloc(num_points * sizeof(double)); 

	for (int i = 0; i < num_points; i++) {

		double product = 1; 

		for (int j = 0; j < num_points; j++) {

			if (i != j) {
				product *= (xo - x[j]) / (x[i] - x[j]);
			}
		}

		L[i] = product; 
	}

	// Compute Lagrange polynomial value
	double f_xo = 0; 

	for (int i = 0; i < num_points; i++) {
		f_xo += L[i] * f[i];
	}

	// Free memory 
	free(L); 

	return f_xo; 
}

/* 
 * Function: interp_cubicspline
 * --------------------
 * Returns the cubic spline interpolation of the function f at the point xo
 */ 
double interp_cubicspline(double* x, double* f, int num_points, double xo) {

    // Make sure we are interpolating (not extrapolating)
    if (xo < x[0] || x[0] > x[num_points-1]) {
        printf("ERROR: evaluation point is not within dataset\n");
        exit(EXIT_FAILURE);
    }

    // Get the number of intervals
    int n = num_points - 1; 

    // Allocate memory
    double* h = safemalloc(n * sizeof(double));
    double* a = safemalloc((n + 1) * sizeof(double)); 
    double* b = safemalloc(n * sizeof(double));
    double* c = safemalloc((n + 1) * sizeof(double)); 
    double* d = safemalloc(n * sizeof(double)); 

    // Caclulate 'h's
    for (int i = 0; i < n; i++) {
        h[i] = x[i+1] - x[i];
    }

    // Find the 'a's
    for (int i = 0; i <= n; i++) {
        a[i] = f[i];
    }

    // Find the 'c's using Thomas algorithm 
    // Allocate memory
    double* u = safemalloc((n + 1) * sizeof(double)); 
    double* v = safemalloc((n + 1) * sizeof(double));
    double* w = safemalloc((n + 1) * sizeof(double)); 
    double* q = safemalloc((n + 1) * sizeof(double));

    // Set up the linear algebraic system
    u[0] = 0; 
    u[n] = 0; 
    v[0] = 1; 
    v[n] = 1; 
    w[0] = 0; 
    w[n] = 0; 
    q[0] = 0; 
    q[n] = 0;

    for (int i = 1; i < n; i++) {
        u[i] = h[i-1]; 
        v[i] = 2 * (h[i-1] + h[i]); 
        w[i] = h[i];
        q[i] = (3 / h[i]) * (a[i+1] - a[i]) + (3 / h[i-1]) * (a[i-1] - a[i]);
    }

    // Solve
    linalgbsys_thomassolve(v, w, u, q, c, n+1); 

    // Free memory
    free(u); 
    free(v); 
    free(w); 
    free(q); 

    // Find the 'b's
    for (int i = 0; i < n; i++) {
        b[i] = (1 / h[i]) * (a[i+1] - a[i]) - (h[i] / 3) * (2 * c[i] + c[i+1]);
    }

    // Find the 'd's
    for (int i = 0; i < n; i++) {
        d[i] = (c[i+1] - c[i]) / (3 * h[i]);
    }

    // Find the correct interval 
    int i = 0; 
    while(i < num_points - 1) {
        if (xo >= x[i] && xo <= x[i+1]) {
            break; 
        }
        i++;
    } 

    // Compute the cubic spline function value at xo
    double f_xo = a[i] + b[i] * (xo - x[i]) + c[i] * pow(xo - x[i], 2) + d[i] * pow(xo - x[i], 3);

    // Free memory
    free(h); 
    free(a);
    free(b); 
    free(c); 
    free(d); 

	return f_xo; 
}

/* 
 * Function: heateqn_integrate_explicit_fe
 * --------------------
 * Explicit integration scheme with fixed ends
 */ 
void heateqn_integrate_explicit_fe(double* f_n, double* f_nplus1, double mu, int Nx, double delta_t) {

    double delta_x = (X_UPPER - X_LOWER) / (double) Nx;

    // Set boundary conditions
    f_nplus1[0] = f_n[0] + delta_t * mu * LOWER_BOUNDARY_CONDITION;
    f_nplus1[Nx] = f_n[Nx] + delta_t * mu * UPPER_BOUNDARY_CONDITION; 

    // Perform the integration
    for (int i = 1; i < Nx; i++) {
        f_nplus1[i] = f_n[i] + delta_t * heateqn_RHS(f_n[i], f_n[i+1], f_n[i-1], delta_x, mu);
    }
} 

/* 
 * Function: heateqn_integrate_explicit_ve
 * --------------------
 * Explicit integration scheme with variable ends
 */ 
void heateqn_integrate_explicit_ve(double* f_n, double* f_nplus1, double mu, int Nx, double delta_t) {

    double delta_x = (X_UPPER - X_LOWER) / (double) Nx;

    // Set boundary conditions
    f_nplus1[0] = f_n[0] + mu * (delta_t * (f_n[0] - 2 * f_n[1] + f_n[2])) / pow(delta_x, 2);
    f_nplus1[Nx] = f_n[Nx] + mu * (delta_t * (f_n[Nx] - 2 * f_n[Nx-1] + f_n[Nx-2])) / pow(delta_x, 2); 

    // Perform the integration
    for (int i = 1; i < Nx; i++) {
        f_nplus1[i] = f_n[i] + delta_t * heateqn_RHS(f_n[i], f_n[i+1], f_n[i-1], delta_x, mu);
    }
} 

/* 
 * Function: heateqn_integrate_implicit_fe
 * --------------------
 * Implicit integration scheme with fixed ends
 */ 
void heateqn_integrate_implicit_fe(double* f_n, double* f_nplus1, double mu, int Nx, double delta_t) {

    double delta_x = (X_UPPER - X_LOWER) / (double) Nx;

    // Set boundary conditions
    f_nplus1[0] = f_n[0] + delta_t * mu * LOWER_BOUNDARY_CONDITION;
    f_nplus1[Nx] = f_n[Nx] + delta_t * mu * UPPER_BOUNDARY_CONDITION;

    // Perform the integration using the Thomas algorithm
    // Allocate memory for the linear alg system
    double* a = safemalloc((Nx - 1) * sizeof(double));
    double* b = safemalloc((Nx - 1) * sizeof(double));
    double* c = safemalloc((Nx - 1) * sizeof(double));
    double* q = safemalloc((Nx - 1) * sizeof(double));

    // Populate the system
    double multiplier = mu * delta_t / pow(delta_x, 2);

    for (int i = 0; i < Nx - 1; i++) {

        if (i == 0) {
            c[i] = 0; 
        } else{
            c[i] = -multiplier; 
        }

        if (i == Nx - 2) {
            b[i] = 0;
        } else {
            b[i] = -multiplier; 
        }

        a[i] = 1 + 2 * multiplier; 
        q[i] = f_n[i+1];
    }

    // Solve the system with the Thomas algorithm 
    double* x = safemalloc((Nx - 1) * sizeof(double)); // solution vector
    linalgbsys_thomassolve(a, b, c, q, x, Nx - 1); 

    // Store the solutions
    for (int i = 1; i < Nx; i++) {
        if (i == 1) {
            f_nplus1[i] = x[i-1] - f_nplus1[0]; 
        } else if (i == Nx - 1) {
            f_nplus1[i] = x[i-1] - f_nplus1[Nx];
        } else {
            f_nplus1[i] = x[i-1]; 
        }
    }

    // Free memory 
    free(x); 
    free(a); 
    free(b); 
    free(c); 
    free(q); 
}

/* 
 * Function: heateqn_RHS
 * --------------------
 * Computes the right hand side of the heat equation
 */ 
double heateqn_RHS(double f_i, double f_iplus1, double f_iminus1, double delta_x, double mu) {
    return (mu * (f_iminus1 - 2 * f_i + f_iplus1)) / pow(delta_x, 2);
}

/* 
 * Function: safemalloc
 * --------------------
 * Safe malloc function that checks the memory allocation was successful before returning 
 * the pointer to the memory.
 * 
 * size: the size of memory required
 * 
 * returns: the pointer to the memory allocation
 */ 
void* safemalloc(size_t size) {
    void* ptr= NULL;

    ptr = malloc(size); 

    if (ptr == NULL) {
        printf("ERROR: %ld bytes could not be allocated.", size);
        exit(EXIT_FAILURE); 
    }

    return ptr; 
}

/* 
 * Function: saferealloc
 * ---------------------
 * Safe realloc function that checks the memory reallocation was successful before returning 
 * the pointer to the memory.
 * 
 * ptr: the pointer to the currently allocated memory
 * size: the size of memory required
 * 
 * returns: the pointer to the memory allocation
 */ 
void* saferealloc(void* ptr, size_t size) {
    ptr = realloc(ptr, size); 

    if (ptr == NULL) {
        printf("ERROR: %ld bytes could not be reallocated.", size);
        exit(EXIT_FAILURE); 
    }

    return ptr; 
}

/* 
 * Function: safefopen
 * -------------------
 * Safe fopen function that checks the opening of the file was successful before returning
 * the pointer to the file.
 * 
 * filename: the name of the file
 * mode: the mode to open the file with ("r" read, "w" write)
 * 
 * returns: the pointer to the file 
 */ 
FILE* safefopen(const char* filename, const char* mode) {
    FILE* file = NULL; 

    file = fopen(filename, mode);

    if (file == NULL) {
        printf("ERROR: file could not be opened.");
        exit(EXIT_FAILURE); 
    }

    return file; 
}

/* 
 * Function: degtorad
 * --------------------
 * Converts angle from degrees to radians
 */ 
double degtorad(double angle) {
    return angle * PI / 180; 
}

/* 
 * Function: degtorad
 * --------------------
 * Converts angle from radians to degrees
 */ 
double radtodeg(double angle) {
    return angle * 180 / PI; 
}

/* 
 * Function: tan
 * --------------------
 * Returns the tangent of the angle x (radians)
 */ 
double tan(double x) {
    return sin(x) / cos(x); 
}

/* 
 * Function: cot
 * --------------------
 * Returns the cotangent of the angle x (radians)
 */ 
double cot(double x) {
    return cos(x) / sin(x); 
}

/* 
 * Function: cosec
 * --------------------
 * Returns the cosecant of the angle x (radians)
 */ 
double cosec(double x) {
    return 1 / sin(x); 
}

